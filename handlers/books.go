package handlers

import (
	"database/sql"
	"gin_backend/models"
)

func CreateBook(book models.Book) error {
	db, err := sql.Open("postgres", "your-database-connection-string")
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec("INSERT INTO books (title, rating, author, genre) VALUES ($1, $2, $3, $4)",
		book.Title, book.Rating, book.Author, book.Genre)
	if err != nil {
		return err
	}

	return nil
}
