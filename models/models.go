package models

type Book struct {
    ID     int
    Title  string
    Rating float32
    Author string
    Genre  string
}