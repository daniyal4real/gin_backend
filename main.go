package main

import (
	"database/sql"
	"gin_backend/handlers"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	connStr := "user=your-username password=your-password dbname=your-database-name sslmode=require"
    db, err := sql.Open("postgres", connStr)
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    // Create a new router
    router := mux.NewRouter()

    // Define your routes
    router.HandleFunc("/books", handlers.CreateBook).Methods("POST")

    // Start the server
    log.Fatal(http.ListenAndServe(":8000", router))
}